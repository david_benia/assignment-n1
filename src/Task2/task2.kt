package Task2

fun isPalindrome(word: String): Boolean {

    val middle = Math.floor((word.length / 2).toDouble()).toInt()

    if(word.length % 2 == 0) {
        return word.slice(0 until middle) == word.slice(middle until word.length).reversed()
    } else
    {
        return word.slice(0 until middle) == word.slice((middle + 1) until word.length).reversed()
    }
}