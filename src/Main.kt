import Task1.*
import Task2.*

fun main() {
    // Task 1
    println("\nTask1\n")
    println("Ar.Mean of elements on even indices: ${evenIndexElementMean(intArrayOf(5, 8, 10, 13, 15, 17, 20, 23, 25))}")

    // Task 2
    println("\nTask2\n")

    val words = arrayOf("test", "kayak", "car", "anna", "mom", "tree", "level", "radar")

    for(i in words){
        println("is $i a palindrome? - ${isPalindrome(i)}")
    }
}