package Task1

fun evenIndexElementMean(list: IntArray): Int {

    var evenSum: Int = 0
    var evenCount: Int = 0

    for(i in list) {
        if(list.indexOf(i) % 2 == 0){
            evenSum += i
            evenCount++
        }
    }

    return evenSum/evenCount
}